/*
 * deomo.cpp: Implementations of a test program for the OpenGL autostereogram renderer
 *
 * Copyright (c) 2018 Matis Granger, Gustave Bainier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * File created on 26/11/18.
 *
*/

#include "demo.h"
#include "openstereo.h"
#include <cmath>
#include <iostream>

GLvoid keyboard(unsigned char touche, int x, int y) {
    // Suivant les touches pressees, nous aurons un comportement different de l'application
    // ESCAPE ou 'q' : fermera l'application
    // 'p' : affichage du carre plein
    // 'f' : affichage du carre en fil de fer
    // 's' : affichage des sommets du carre
    switch (touche) {
        case 'p' : // carre plein
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
            break;
        case 'f' : // fil de fer
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            break;
        case 's' : // sommets du carre
            glPolygonMode(GL_FRONT_AND_BACK, GL_POINT);
            break;

            // Gestion du tampon de profondeur
        case 'd' :
            // TODO : activer le test du tampon de profondeur
            glutPostRedisplay();
            break;
        case 'D' :
            //TODO : desactiver le test du tampon de profondeur
            glutPostRedisplay();
            break;
        case 'q' :
            exit(0);
    }

    // Demande a GLUT de reafficher la scene
    glutPostRedisplay();
}

GLvoid reshape(int w, int h) {
    /* // Projection
     glMatrixMode(GL_PROJECTION);

     // Resetting matrix
     glLoadIdentity();

     // Viewport

     glViewport(0, 0, w, h);

     // Mise en place de la perspective
     // TODO : trouver les bons param�tre
     //gluPerspective(focale, 4/3.0, near, far);

     // Placement de la cam�ra
     // TODO : bons param�tres ?
     gluLookAt(0, 0, 2, 0, 0, 0, 0, 1, 0);

     // Retourne a la pile modelview
     glMatrixMode(GL_MODELVIEW);*/
}

int mouseposx;
int mouseposy;
int mouseaccumx[3] = {};  //left, middle, right
int mouseaccumy[3] = {};  //left, middle, right
int mousebutton;  //probably GLUT_LEFT_BUTTON, GLUT_RIGHT_BUTTON, or GLUT_MIDDLE_BUTTON
int mousechangex;
int mousechangey;

// Fonction de rappel de la souris
GLvoid mouse(int bouton, int etat, int x, int y) {
    mouseposx = x;
    mouseposy = y;

    mousechangex = mousechangey = 0;

    mousebutton = bouton;
}

GLvoid motion(int x, int y) {
    mousechangex = x - mouseposx;
    mousechangey = y - mouseposy;
    mouseposx = x;
    mouseposy = y;
    int index = 0;
    if (mousebutton == GLUT_LEFT_BUTTON)
        index = 0;
    else if (mousebutton == GLUT_MIDDLE_BUTTON)
        index = 1;
    else if (mousebutton == GLUT_RIGHT_BUTTON)
        index = 2;
    else
        return;
    mouseaccumx[index] += mousechangex;
    mouseaccumy[index] += mousechangey;
    //glutPostRedisplay();
}

bool getMouseAccumX(int button, int *accumptr) {
    switch (button) {
        case GLUT_LEFT_BUTTON :
            *accumptr = mouseaccumx[0];
            return true;
        case GLUT_MIDDLE_BUTTON :
            *accumptr = mouseaccumx[1];
            return true;
        case GLUT_RIGHT_BUTTON :
            *accumptr = mouseaccumx[2];
            return true;
        default :
            std::cerr << "accum is not stored for that button" << std::endl;
            return false;
    }
}

bool getMouseAccumY(int button, int *accumptr) {
    switch (button) {
        case GLUT_LEFT_BUTTON :
            *accumptr = mouseaccumy[0];
            return true;
        case GLUT_MIDDLE_BUTTON :
            *accumptr = mouseaccumy[1];
            return true;
        case GLUT_RIGHT_BUTTON :
            *accumptr = mouseaccumy[2];
            return true;
        default :
            std::cerr << "accum is not stored for that button" << std::endl;
            return false;
    }
}

int getMouseAccumX(int button) {
    int temp = 0;
    getMouseAccumX(button, &temp);
    return temp;
}

int getMouseAccumY(int button) {
    int temp = 0;
    getMouseAccumY(button, &temp);
    return temp;
}

float getMouseAngleX(int button, int revint, float revfloat) {
    if ((revint <= 0) || (revfloat <= 0))
        return 0.0f;
    int temp = getMouseAccumX(button) % revint;
    if (temp < 0)
        temp = revint + temp;
    return (temp / (float) revint) * revfloat;
}

float getMouseAngleY(int button, int revint, float revfloat) {
    if ((revint <= 0) || (revfloat <= 0))
        return 0.0f;
    int temp = getMouseAccumY(button) % revint;
    if (temp < 0)
        temp = revint + temp;
    return (temp / (float) revint) * revfloat;
}

void recursiveCircles(GLfloat radius, GLfloat startingradius, int xd, int yd, int zd, int parentdir, int numrecs,
                      int maxrecs) {
    if (numrecs > maxrecs)
        return;

    glPushMatrix();
    GLfloat offset = 3 * radius;

    switch (parentdir) {
        case 1:
            glTranslatef(offset, 0.0f, 0.0f);
            glRotatef(0, 1.0f, 0.0f, 0.0f);
            break;
        case 2:
            glTranslatef(-offset, 0.0f, 0.0f);
            glRotatef(0, -1.0f, 0.0f, 0.0f);
            break;
        case 3:
            glTranslatef(0.0f, offset, 0.0f);
            glRotatef(0, 0.0f, 1.0f, 0.0f);
            break;
        case 4:
            glTranslatef(0.0f, -offset, 0.0f);
            glRotatef(0, 0.0f, -1.0f, 0.0f);
            break;
        case 5:
            glTranslatef(0.0f, 0.0f, offset);
            glRotatef(0, 0.0f, 0.0f, 1.0f);
            break;
        case 6:
            glTranslatef(0.0f, 0.0f, -offset);
            glRotatef(0, 0.0f, 0.0f, -1.0f);
            break;
        default:
            glRotatef(0, 0.0f, 1.0f, 0.0f);
            break;
    }

    GLfloat tempcolor[3] = {static_cast<GLfloat>(xd), static_cast<GLfloat>(yd), static_cast<GLfloat>(zd)};
    for (float &i : tempcolor) {
        i = 1 - abs(i) / (4 * startingradius);
        i = i * i;
        i = 0.8f * i * i;
        i *= 0.5 + 0.5 * (1 - numrecs / (float) maxrecs);
    }

    glColor3f(tempcolor[0], tempcolor[1], 0.1f + 0.9f * tempcolor[2]);

    int polydetail = static_cast<int>(5 + 10 * (radius / startingradius));

    glutSolidSphere(radius, polydetail, polydetail);

    GLfloat nextradius = radius / 2;
    GLfloat nextcenter = radius + nextradius;
    if (parentdir != 1)  //x neg
        recursiveCircles(nextradius, startingradius, xd - nextcenter, yd, zd, 2, numrecs + 1, maxrecs);
    if (parentdir != 2)  //x pos
        recursiveCircles(nextradius, startingradius, xd + nextcenter, yd, zd, 1, numrecs + 1, maxrecs);
    if (parentdir != 3)  //y neg
        recursiveCircles(nextradius, startingradius, xd, yd - nextcenter, zd, 4, numrecs + 1, maxrecs);
    if (parentdir != 4)  //y pos
        recursiveCircles(nextradius, startingradius, xd, yd + nextcenter, zd, 3, numrecs + 1, maxrecs);
    if (parentdir != 5)  //z neg
        recursiveCircles(nextradius, startingradius, xd, yd, zd - nextcenter, 6, numrecs + 1, maxrecs);
    if (parentdir != 6)  //z pos
        recursiveCircles(nextradius, startingradius, xd, yd, zd + nextcenter, 5, numrecs + 1, maxrecs);
    glPopMatrix();
}

void beginRecursiveCircles(GLfloat radius, int maxrecs) {
    recursiveCircles(radius, radius, 0, 0, 0, 0, 0, maxrecs);
}

GLvoid display() { // Le swap buffer est d�j� appel� par la fonction stereogramDisplay
    /*glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);

    glLoadIdentity();*/

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef(0.0f, 0.0f, -20.0f);
    glRotatef(getMouseAngleY(GLUT_RIGHT_BUTTON, getWinYRes(), 360.0),
              1.0f, 0.0f, 0.0f);
    glRotatef(getMouseAngleY(GLUT_LEFT_BUTTON, (int) (getWinYRes() / 0.2), 360.0),
              1.0f, 0.0f, 0.0f);
    glRotatef(getMouseAngleX(GLUT_RIGHT_BUTTON, getWinXRes(), 360.0),
              0.0f, 1.0f, 0.0f);
    glRotatef(getMouseAngleX(GLUT_LEFT_BUTTON, (int) (getWinXRes() / 0.2), 360.0),
              0.0f, 1.0f, 0.0f);

    beginRecursiveCircles(5.0f, 3);//glutSolidTeapot(.5);
    glPopMatrix();

    /*glFlush();
    glutSwapBuffers();*/
}
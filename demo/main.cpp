/*
 * main.cpp: Example program for the OpenGL autostereogram renderer
 *
 * Copyright (c) 2018 Matis Granger, Gustave Bainier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * File created on 10/10/18.
 *
*/

#include "demo.h"
#include "openstereo.h"

int main(int argc, char **argv) {
    stereogramInit(&argc, argv);

    glEnable(GL_DEPTH_TEST);

    // Définition de la couleur d'effacement du framebuffer
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

    glutDisplayFunc(stereogramDisplay(display));
    glutReshapeFunc(stereogramReshape(reshape));
    glutKeyboardFunc(stereogramKeyboard(keyboard));
    glutMotionFunc(stereogramMotion(motion));
    glutMouseFunc(stereogramMouse(mouse));

    glutMainLoop();

    stereogramDestroy();
    return EXIT_SUCCESS;
}

# OpenStereo: OpenGL autostereogram renderer

## About

This program was developed by Matis Granger and Gustave Bainier, two students of Ecole Centrale de Nantes in virtual reality major.
Its purpose is to do auto stereogram real-time rendering using shaders. It is adapted from MaGLi, originally written by Daniel Schroeder and published at [danielschroeder.me](http://danielschroeder.me/projects/magliv2.html)
It was rewritten to be portable and modular enough to be included without much headaches into existing OpenGL GLUT programs.

## Building

We adapted the code to do CMake. All you have to do is configure CMake and choose what you want to build.

### Step 1: Library Compilation

We recommend you create a new directory for compilation:

    mkdir build
    cd build/

**Static Library Compilation**

To compile the static library, the BUILD_STATIC_LIBS must be set to "ON" (it is by default). This can be done by adding the `-DBUILD_STATIC_LIBS=ON` flag when generating Makefiles:

    cmake -DBUILD_STATIC_LIBS=ON ..


**Shared Library Compilation**

To compile the static library, the BUILD_SHARED_LIBS must be set to "ON" (it is **not** by default). This can be done by adding the `-DBUILD_SHARED_LIBS=ON` flag when generating Makefiles:

    cmake -DBUILD_SHARED_LIBS=ON ..

**Example Program Compilation**

To compile the static library, the BUILD_EXAMPLE must be set to "ON" (it is **not** by default). This can be done by adding the `-DBUILD_EXAMPLE=ON` flag when generating Makefiles:

    cmake -DBUILD_EXAMPLE=ON ..

### Step 2: Actual fun

When the files are generated, simply run make to build the library and/or example:

    make

### Step 3: Working with the library

The library easily integrates into any existing OpenGL project. Here is an example with a dead simple GLUT code:
 
    #include <openstereo.h>
    #include <GL/glut.h>
    
    int main(int argc, char *argv[]) {
        stereogramInit(&argc, argv);
        
        // ... your own code (set GL booleans and settings...)
    
        glutDisplayFunc(stereogramDisplay(display));
        glutReshapeFunc(stereogramReshape(reshape));
        glutKeyboardFunc(stereogramKeyboard(keyboard));
        glutMotionFunc(stereogramMotion(motion));
        glutMouseFunc(stereogramMouse(mouse));
    
        glutMainLoop();
        
        // ... your own cleanup handlers
    
        stereogramDestroy();
        return EXIT_SUCCESS;
    }
    
See the demo code for a concrete integration of the library.


## Running the example

If the example was compiled (see above), you can run it from the build dir using:

    ./openstereo_example
	
** You may need to copy the shaders from the `dist` folder first! **

### Shortcuts

Some shortcuts are available thanks to stereogramKeyboard() if compiled with `OPENSTEREO_DEV=ON`:

Before using any of them, you first have to enable the development mode of OpenStereo by using `<` key. Then you can use any of the following:

- `q`: Quit the application cleanly destroying the library
- `r`: Invert right/left rendering (flip eyes)
- `0`: Display mode 1: stereogram
- `1`: Display mode 2: normal
- `2`: Display mode 3: height map

## License

Just like MaGLi, OpenStereo is licensed under GNU GPL v3. Details of the license can be found in the file [LICENSE.txt](LICENSE.txt).
/*
 * framebuffer_utils.cpp: Utilities for OpenGL framebuffers
 *
 * Copyright (c) 2018 Matis Granger, Gustave Bainier
 * Copyright (c) 2011 Daniel Schroeder
 *
 * This file is adapted from MaGLi.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * File created on 10/10/18.
 *
*/

#include "framebuffer/framebuffer_utils.h"
#include "utils/log.h"
#include <iostream>
#include <assert.h>

using namespace std;

int generate_depth_texture(GLuint *texobjptr, int width, int height) {
    assert(texobjptr);
    auto tempfloatptr = new(std::nothrow) float[width * height];
    if (!tempfloatptr) {
        *texobjptr = 0;
        log_error << "Texture dimension are not within acceptable for call to generate_depth_texture()" << endl;
        delete[] tempfloatptr;
        return 1;
    }
    //fill it with something interesting
    for (int j = 0; j < height; j++)
        for (int i = 0; i < width; i++)
            tempfloatptr[j * width + i] = (((i + j) % 8) & 4) ? 1.0f : 0.0f;

    glPushAttrib(GL_TEXTURE_BIT);
    glGenTextures(1, texobjptr);
    glBindTexture(GL_TEXTURE_2D, *texobjptr);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, tempfloatptr);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    glPopAttrib();

    delete[] tempfloatptr;
    return 0;
}

int
generate_texture_2d(GLuint *texobjptr, GLint internalformat, GLsizei width, GLsizei height, float *data) {
    assert(texobjptr);
    if (!data) {
        *texobjptr = 0;
        log_error << "generate_texture_2d() received invalid data" << endl;
        return 2;
    }
    glPushAttrib(GL_TEXTURE_BIT);
    glGenTextures(1, texobjptr);
    glBindTexture(GL_TEXTURE_2D, *texobjptr);
    glTexImage2D(GL_TEXTURE_2D, 0, internalformat, width, height, 0, GL_RGB, GL_FLOAT, data);
    glPopAttrib();

    return 0;
}

int generate_texture_2d(GLuint *texobjptr, int width, int height, float *bufptr) {
    assert(texobjptr);
    if (!bufptr) {
        *texobjptr = 0;
        log_error << "generate_texture_2d() received invalid data" << endl;
        return 2;
    };

    glPushAttrib(GL_TEXTURE_BIT);
    glGenTextures(1, texobjptr);
    glBindTexture(GL_TEXTURE_2D, *texobjptr);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_FLOAT, bufptr);
    glPopAttrib();

    glPushAttrib(GL_TEXTURE_BIT);
    glBindTexture(GL_TEXTURE_2D, *texobjptr);

    // set some reasonable texture parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    glPopAttrib();

    return 0;
}

int generate_texture_2d(GLuint *texobjptr, int width, int height) {
    auto tempfloatptr = new(std::nothrow) float[3 * width * height];
    if (!tempfloatptr) {
        *texobjptr = 0;
        log_error << "allocation failed in generate_texture_2d" << std::endl;
        return 1;
    }
    for (int j = 0; j < height; j++) {
        for (int i = 0; i < width; i++) {
            tempfloatptr[3 * (j * width + i) + 0] =
            tempfloatptr[3 * (j * width + i) + 1] =
            tempfloatptr[3 * (j * width + i) + 2] =
                    (((i + j) % 8) & 4) ? 1.0f : 0.0f;
        }
    }

    int ret = generate_texture_2d(texobjptr, width, height, tempfloatptr);
    delete[] tempfloatptr;

    return ret;
}

int generate_texture_rgba(GLuint *texobjptr, int width, int height) {
    assert(texobjptr);
    auto tempfloatptr = new(std::nothrow) float[3 * width * height];
    if (!tempfloatptr) {
        *texobjptr = 0;
        log_error << "allocation failed in generate_texture_rgba" << std::endl;
        return 1;
    }
    for (int j = 0; j < height; j++) {
        for (int i = 0; i < width; i++) {
            tempfloatptr[3 * (j * width + i) + 0] =
            tempfloatptr[3 * (j * width + i) + 1] =
            tempfloatptr[3 * (j * width + i) + 2] =
                    (((i + j) % 8) & 4) ? 1.0f : 0.0f;
        }
    }

    int ret = generate_texture_rgba(texobjptr, width, height, tempfloatptr);
    delete[] tempfloatptr;

    return ret;
}

int generate_texture_rgba(GLuint *texobjptr, int width, int height, float *data) {
    assert(texobjptr);
    if (!data) {
        *texobjptr = 0;
        log_error << "generate_texture_rgba() received invalid data" << endl;
        return 2;
    }

    GLuint tempint;
    if (0 == generate_texture_2d(&tempint, GL_RGBA16F_ARB, width, height, data)) {
        *texobjptr = tempint;
        glPushAttrib(GL_TEXTURE_BIT);
        glBindTexture(GL_TEXTURE_2D, tempint);

        //set some reasonable texture parameters
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

        glPopAttrib();

        return 0;
    }
    *texobjptr = 0;
    return 1;
}

int create_render_buffer(GLuint *rbobjptr, GLenum internalformat, int width, int height) {
    assert(rbobjptr);
    GLuint prevrenderbuf;
    if (!GLEW_EXT_framebuffer_object) {
        log_error << "framebuffers are NOT supported by this implementation of GLEW!" << endl;
        *rbobjptr = 0;
        return 1;
    }
    glGetIntegerv(GL_RENDERBUFFER_BINDING_EXT, (GLint *) &prevrenderbuf);
    glGenRenderbuffersEXT(1, rbobjptr);
    glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, *rbobjptr);
    glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT, internalformat, width, height);
    glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, prevrenderbuf);
    return 0;
}

int create_framebuffer(GLuint *fbobjptr) {
    assert(fbobjptr);
    GLuint prevframebuf;
    if (!GLEW_EXT_framebuffer_object) {
        log_error << "framebuffers are NOT supported by this implementation of GLEW!" << endl;
        *fbobjptr = 0;
        return 1;
    }
    glGetIntegerv(GL_FRAMEBUFFER_BINDING_EXT, (GLint *) &prevframebuf);
    glGenFramebuffersEXT(1, fbobjptr);
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, *fbobjptr);
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, prevframebuf);
    return 0;
}

int create_textured_framebuffer(GLuint *fbobjptr, GLuint *deptexptr, GLuint *texobjptr, int width, int height) {
    assert(fbobjptr);
    GLuint lastfbo;
    int ret;

    if ((ret = generate_depth_texture(deptexptr, width, height)) != 0) {
        cerr << "simple framebuffer generation: depth texture gen failed" << endl;
        *fbobjptr = *deptexptr = *texobjptr = 0;
        return ret;
    }
    if ((ret = generate_texture_2d(texobjptr, width, height)) != 0) {
        cerr << "simple framebuffer generation: texture gen failed" << endl;
        glDeleteTextures(1, deptexptr);
        *fbobjptr = *deptexptr = *texobjptr = 0;
        return ret;
    }
    if ((ret = create_framebuffer(fbobjptr)) != 0) {
        cerr << "simple framebuffer generation: framebuffer gen failed" << endl;
        glDeleteTextures(1, deptexptr);
        glDeleteTextures(1, texobjptr);
        *fbobjptr = *deptexptr = *texobjptr = 0;
        return ret;
    }

    glGetIntegerv(GL_FRAMEBUFFER_BINDING_EXT, (GLint *) &lastfbo);
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, *fbobjptr);
    glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_TEXTURE_2D, *deptexptr, 0);
    glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, *texobjptr, 0);

    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, lastfbo);
    return 0;
}

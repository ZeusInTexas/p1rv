/*
 * renderer.cpp: OpenGL autostereogram rendering
 *
 * Copyright (c) 2018 Matis Granger, Gustave Bainier
 * Copyright (c) 2011 Daniel Schroeder
 *
 * This file is adapted from MaGLi.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * File created on 16/11/18.
 *
*/

#if _WIN32

#include <Windows.h>

#endif

#include <GL/glew.h>
#include <GL/glut.h>

#include "utils/log.h"
#include "render/renderer.h"

#include <math.h>
#include <framebuffer/framebuffer_utils.h>
#include <shaders/shaders_utils.h>

#define shader_load_error(V) log_error << "could not load shader: " << V << std::endl;
#undef near

// === Variables
bool is_init = false;

// Shaders handles
GLuint *rightStripTextures = nullptr;
GLuint *leftStripTextures = nullptr;
GLuint seedTextures[2] = {};
GLuint gapTexture;
GLuint stripFBO;
GLuint stripRBO;
GLuint rightSkewFBO;
GLuint rightSkewDepth;
GLuint rightSkewTexture;
GLuint leftSkewFBO;
GLuint leftSkewDepth;
GLuint leftSkewTexture;
GLuint rightOneWayVS;
GLuint rightOneWayFS;
GLuint rightOneWayProgram;
GLuint leftOneWayVS;
GLuint leftOneWayFS;
GLuint leftOneWayProgram;
GLuint rightMapTexVS;
GLuint rightMapTexFS;
GLuint rightMapTexProgram;
GLuint leftMapTexVS;
GLuint leftMapTexFS;
GLuint leftMapTexProgram;
GLuint seedFillerVS;
GLuint seedFillerFS;
GLuint seedFillerProgram;
GLuint mixerVS;
GLuint mixerFS;
GLuint mixerProgram;

int stripWidth = 75;
int stripHeight = 400;
int startingStrips = 10;
int lenStripTextures = -1;

bool renderRightOneWay = true;

// Functions static storage
GLUT_displayFunc stereogramDisplayFunc = nullptr;

void setProjection(float shiftMultiplier) {
    //set the projection for the skewing

    //In openGL, subsequent multiplications are multiplied
    //on to the right of the existing matrix
    //as in, moving from A to AB

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    int relevantStrips = startingStrips + 6;
    int effectiveWidth = stripWidth * relevantStrips;

    int fov = 60;
    float pi = 3.1415926535f;
    float zNear = 5.0;
    float zFar = 100.0;

    //calculate shiftamount - the amount in eye coords to move
    float f = static_cast<float>(1.0 / tan((fov / 2.0) * (2.0 * pi / 360)));
    float aspect = float(effectiveWidth) / stripHeight;
    float shiftAmount = static_cast<float>(-(2 * aspect) /
                                           (relevantStrips * f * (1.0 / zNear - 1.0 / zFar)));

    shiftAmount *= shiftMultiplier;

    //finally, shift the far depth out by the appropriate amount
    //shift by shiftmultiplier * texwidthndc (texwidthndc = 4.0 / PRJ_VS)
    glTranslatef(shiftMultiplier * 4.0f / relevantStrips, 0.0f, 0.0f);

    //(second to) last thing to be applied is the shift in ncd to keep zfar
    //in place.
    //in ndc, something at PRJ_ZFAR is shifted by
    //((f/aspect) * shiftamount) / PRJ_ZFAR, so undo this
    glTranslatef(-((f / aspect) * shiftAmount) / zFar, 0.0f, 0.0f);


    //apply projection
    gluPerspective(fov, aspect, zNear, zFar);
    //do the shiftamount
    glTranslatef(shiftAmount, 0.0f, 0.0f);
}

bool computeSkews(GLUT_displayFunc fun) {
    //viewport
    glViewport(0, 0, stripWidth * (startingStrips + 6), stripHeight);

    //right and left skews
    GLuint lastFBO;
    glGetIntegerv(GL_FRAMEBUFFER_BINDING_EXT, (GLint *) &lastFBO);
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, rightSkewFBO);
    setProjection(0.5f);

    fun();

    glFlush();
    glDrawBuffer(GL_COLOR_ATTACHMENT0_EXT);
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, leftSkewFBO);
    setProjection(-0.5f);

    fun();

    glFlush();
    glDrawBuffer(GL_COLOR_ATTACHMENT0_EXT);
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, lastFBO);

    return true;
}

bool uniformFloat(GLuint program, const char *name, float n) {
    GLuint oldprogram;
    glGetIntegerv(GL_CURRENT_PROGRAM, (GLint *) &oldprogram);
    glUseProgram(program);
    //do this thing
    GLint unifloc = glGetUniformLocation(program, (const GLchar *) name);
    if (unifloc < 0)  //the uniform doesn't exist
    {
        log_error << "uniform " << name << " doesn't exist" << std::endl;
        glUseProgram(oldprogram);
        return false;
    }
    glUniform1f(unifloc, n);
    glUseProgram(oldprogram);
    return true;
}

bool uniformSampler2D(GLuint program, const char *name, int value) {
    GLuint oldprogram;
    glGetIntegerv(GL_CURRENT_PROGRAM, (GLint *) &oldprogram);
    glUseProgram(program);
    //do this thing
    GLint unifloc = glGetUniformLocation(program, (const GLchar *) name);
    if (unifloc < 0)  //the uniform doesn't exist
    {
        log_error << "uniform " << name << " doesn't exist" << std::endl;
        glUseProgram(oldprogram);
        return false;
    }
    glUniform1i(unifloc, value);
    glUseProgram(oldprogram);
    return true;
}

GLuint getNearTexture(int stripIndex, bool right) {
    //gives the appropriate texture
    if ((stripIndex < 0) || (stripIndex >= lenStripTextures)) {
        log_error << "stripIndex out of range" << std::endl;
        return 0;
    }
    if (right) {
        //seeks back to farther to the left
        if (stripIndex == 0)
            return seedTextures[1];
        else
            return rightStripTextures[stripIndex - 1];
    } else {
        //seeks back to farther to the right
        if (stripIndex == lenStripTextures - 1)
            return seedTextures[0];
        else
            return leftStripTextures[stripIndex + 1];
    }
}

GLuint getFarTexture(int stripIndex, bool right) {
    //gives the appropriate texture
    if ((stripIndex < 0) || (stripIndex >= lenStripTextures)) {
        log_error << "stripIndex out of range" << std::endl;
        return 0;
    }
    if (right) {
        //seeks back to farther to the left
        if (stripIndex == 0)
            return seedTextures[0];
        else if (stripIndex == 1)
            return seedTextures[1];
        else
            return rightStripTextures[stripIndex - 2];
    } else {
        //seeks back to farther to the right
        if (stripIndex == lenStripTextures - 1)
            return seedTextures[1];
        else if (stripIndex == lenStripTextures - 2)
            return seedTextures[0];
        else
            return leftStripTextures[stripIndex + 2];
    }
}

bool checkProgramStatus(GLuint program) {
    const int MAX_INFO_LOG_SIZE = 400;
    GLint success;
    if (!GLEW_EXT_framebuffer_object || !GLEW_ARB_shading_language_100 || !GLEW_ARB_shader_objects) {
        log_error << "failed to check program status" << std::endl;
        return false;
    }
    GLboolean isprogram = glIsProgram(program);
    if (isprogram == GL_FALSE) {
        log_error << "invalid program - program is not a program" << std::endl;  //yay confusion
        return false;
    }
    //validate the program
    glValidateProgram(program);
    glGetProgramiv(program, GL_VALIDATE_STATUS, &success);
    if (!success) {
        log_error << "validation of program failed" << std::endl;
        GLchar infolog[MAX_INFO_LOG_SIZE];
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &success);
        if (success > MAX_INFO_LOG_SIZE) {
            log_error << "...and info log is too big" << std::endl;
        }
        glGetProgramInfoLog(program, MAX_INFO_LOG_SIZE, nullptr, infolog);
        log_error << "----------------------------------------" << std::endl;
        log_error << infolog << std::endl;
        log_error << "----------------------------------------" << std::endl;
        //don't delete the program - this may tell the user to change uniform values
        //or something
        return false;
    }
    //success!
    return true;
}

bool renderStrip(int stripIndex, bool right) {
    if ((stripIndex < 0) || (stripIndex >= lenStripTextures)) {
        log_error << "stripIndex out of range" << std::endl;
        return false;
    }

    bool uniformSuccess = true;

    //set the texture in stripFBO
    GLuint *stripTextures = (right ? rightStripTextures : leftStripTextures);

    GLuint lastFBO;
    glGetIntegerv(GL_FRAMEBUFFER_BINDING_EXT, (GLint *) &lastFBO);
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, stripFBO);
    glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, stripTextures[stripIndex],
                              0);
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, lastFBO);

    //bind and do nifty stuff with FBO
    GLuint lastProgram;
    glGetIntegerv(GL_CURRENT_PROGRAM, (GLint *) &lastProgram);

    GLuint chosenProgram = right ? rightOneWayProgram : leftOneWayProgram;

    glActiveTexture(GL_TEXTURE0); // nearTex
    glBindTexture(GL_TEXTURE_2D, getNearTexture(stripIndex, right));

    glEnable(GL_TEXTURE_2D);
    glActiveTexture(GL_TEXTURE1); // farTex
    glBindTexture(GL_TEXTURE_2D, getFarTexture(stripIndex, right));

    glEnable(GL_TEXTURE_2D);
    glActiveTexture(GL_TEXTURE2); // depthTex
    glBindTexture(GL_TEXTURE_2D, (right ? rightSkewDepth
                                        : leftSkewDepth));

    glEnable(GL_TEXTURE_2D);
    glActiveTexture(GL_TEXTURE3); // otherDepthTex
    glBindTexture(GL_TEXTURE_2D, (right ? leftSkewDepth
                                        : rightSkewDepth));


    glEnable(GL_TEXTURE_2D);
    glActiveTexture(GL_TEXTURE4); // gapTex  (??)
    glBindTexture(GL_TEXTURE_2D, gapTexture);


    glEnable(GL_TEXTURE_2D);

    //uniforms
    if (!uniformSampler2D(chosenProgram, "nearTex", 0)) {
        log_error << "assigning uniform nearTex" << std::endl;
        uniformSuccess = false;
    }
    if (!uniformSampler2D(chosenProgram, "farTex", 1)) {
        log_error << "assigning uniform farTex" << std::endl;
        uniformSuccess = false;
    }
    if (!uniformSampler2D(chosenProgram, "depthTex", 2)) {
        log_error << "assigning uniform depthTex" << std::endl;
        uniformSuccess = false;
    }
    if (!uniformSampler2D(chosenProgram, "otherDepthTex", 3)) {
        log_error << "assigning uniform otherDepthTex" << std::endl;
        uniformSuccess = false;
    }
    if (!uniformFloat(chosenProgram, "texWidthDepth",
                      2.0f / (startingStrips + 6))) {
        log_error << "assigning uniform depthTex" << std::endl;
        uniformSuccess = false;
    }
    if (!uniformSampler2D(chosenProgram, "gapTex", 4)) {
        log_error << "assigning uniform gapTex" << std::endl;
        uniformSuccess = false;
    }

    if (!checkProgramStatus(chosenProgram)) {
        log_error << "error with program status" << std::endl;
        uniformSuccess = false;
    }

    if (!uniformSuccess) {
        //return to happy state
        glDisable(GL_TEXTURE_2D);
        glActiveTexture(GL_TEXTURE3);
        glDisable(GL_TEXTURE_2D);
        glActiveTexture(GL_TEXTURE2);
        glDisable(GL_TEXTURE_2D);
        glActiveTexture(GL_TEXTURE1);
        glDisable(GL_TEXTURE_2D);
        glActiveTexture(GL_TEXTURE0);
        glDisable(GL_TEXTURE_2D);
        return false;
    }

    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, stripFBO);
    glUseProgram(chosenProgram);

    glViewport(0, 0, stripWidth, stripHeight);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0.0, 1.0, 0.0, 1.0, 0.5, 1.5);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    //draw
    glColor3f(1.0f, 1.0f, 1.0f);
    float startx = (2 + stripIndex) / float(startingStrips + 6);
    float endx = (2 + stripIndex + 1) / float(startingStrips + 6);
    glBegin(GL_QUADS);
    glMultiTexCoord2f(GL_TEXTURE0, startx, 0.0f);
    glMultiTexCoord2f(GL_TEXTURE1, 0.0f, 0.0f);
    glVertex3f(0.0f, 0.0f, -1.0f);
    glMultiTexCoord2f(GL_TEXTURE0, endx, 0.0f);
    glMultiTexCoord2f(GL_TEXTURE1, 1.0f, 0.0f);
    glVertex3f(1.0f, 0.0f, -1.0f);
    glMultiTexCoord2f(GL_TEXTURE0, endx, 1.0f);
    glMultiTexCoord2f(GL_TEXTURE1, 1.0f, 1.0f);
    glVertex3f(1.0f, 1.0f, -1.0f);
    glMultiTexCoord2f(GL_TEXTURE0, startx, 1.0f);
    glMultiTexCoord2f(GL_TEXTURE1, 0.0f, 1.0f);
    glVertex3f(0.0f, 1.0f, -1.0f);
    glEnd();

    glFlush();
    glDrawBuffer(GL_COLOR_ATTACHMENT0_EXT);

    glDisable(GL_TEXTURE_2D);
    glActiveTexture(GL_TEXTURE3);
    glDisable(GL_TEXTURE_2D);
    glActiveTexture(GL_TEXTURE2);
    glDisable(GL_TEXTURE_2D);
    glActiveTexture(GL_TEXTURE1);
    glDisable(GL_TEXTURE_2D);
    glActiveTexture(GL_TEXTURE0);
    glDisable(GL_TEXTURE_2D);

    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, lastFBO);
    glUseProgram(lastProgram);

    return true;
}

bool computeOneWay(bool right) {
    if (right) {
        for (int i = 0; i < lenStripTextures; i++) {
            renderStrip(i, true);
        }
    } else {
        for (int i = lenStripTextures - 1; i >= 0; i--) {
            renderStrip(i, false);
        }
    }
    return true;
}

/*
	### INIT STUFF ###
*/

int reset_StripTextures();

int init_StripTextures() {
    if (lenStripTextures > 0) // Just ensure it's been reset...
        reset_StripTextures();

    int ret;
    //make strip textures
    rightStripTextures = new(std::nothrow) GLuint[startingStrips + 2];
    if (!rightStripTextures) {
        log_error << "FAIL: memory allocation" << std::endl;
        return 1;
    }
    leftStripTextures = new(std::nothrow) GLuint[startingStrips + 2];
    if (!leftStripTextures) {
        log_error << "FAIL: memory allocation" << std::endl;
        return 1;
    }
    lenStripTextures = startingStrips + 2;
    for (int i = 0; i < lenStripTextures; i++) {
        rightStripTextures[i] = 0;
        leftStripTextures[i] = 0;
    }
    for (int i = 0; i < lenStripTextures; i++) {
        if (0 != (ret = generate_texture_rgba(&(rightStripTextures[i]), stripWidth, stripHeight))) {
            log_error << "could NOT generate strip textures: R" << std::endl;
            glDeleteTextures(lenStripTextures, rightStripTextures);
            delete[] rightStripTextures;
            rightStripTextures = nullptr;
            lenStripTextures = -1;
            return ret;
        }
        if (0 != (ret = generate_texture_rgba(&(leftStripTextures[i]), stripWidth, stripHeight))) {
            log_error << "could NOT generate strip textures: L" << std::endl;
            glDeleteTextures(lenStripTextures, rightStripTextures);
            glDeleteTextures(lenStripTextures, leftStripTextures);
            delete[] rightStripTextures;
            delete[] leftStripTextures;
            rightStripTextures = nullptr;
            leftStripTextures = nullptr;
            lenStripTextures = -1;
            return ret;
        }
    }
    return 0;
}

int reset_StripTextures() {
    //delete existing
    glDeleteTextures(lenStripTextures, rightStripTextures);
    glDeleteTextures(lenStripTextures, leftStripTextures);
    delete[] rightStripTextures;
    delete[] leftStripTextures;
    rightStripTextures = nullptr;
    leftStripTextures = nullptr;
    lenStripTextures = -1;
    return init_StripTextures();
}

int init_SeedTextures() {
    int ret;
    float *randomData;
    for (unsigned int &seedTexture : seedTextures) {
        /*
            // If we are using a texture from a file
            int tempx, tempy;
            char * fileName = (char * ) (i == 0 ? "../play/wider_left.ppm" : "../play/wider_right.ppm");
            bool success = loadPNM(fileName, &randomData, &tempx, &tempy, stripWidth, stripHeight);
        */

        //make rand
        randomData = new float[3 * stripWidth * stripHeight];
        for (int j = 0; j < 3 * stripWidth * stripHeight; j++) {
            randomData[j] = rand() / float(RAND_MAX);
        }

        if (0 != (ret = generate_texture_rgba(&seedTexture, stripWidth, stripHeight, randomData))) {
            log_error << "could NOT generate seed textures" << std::endl;
            glDeleteTextures(2, seedTextures);
            return ret;
        }
        delete[] randomData;
    }
    return 0;
}

int reset_SeedTextures() {
    //remove them
    glDeleteTextures(2, seedTextures);
    return init_SeedTextures();
}

int init_GapTexture() {
    int ret;
    //make random filler
    auto randomData = new(std::nothrow) float[3 * stripWidth * (startingStrips + 2) * stripHeight];
    if (!randomData) {
        log_error << "allocation failed in init_GapTexture" << std::endl;
        return 1;
    }
    for (int i = 0; i < 3 * stripWidth * (startingStrips + 2) * stripHeight; i++) {
        randomData[i] = rand() / float(RAND_MAX);
    }
    if (0 != (ret = generate_texture_2d(&gapTexture, stripWidth * (startingStrips + 2), stripHeight, randomData))) {
        log_error << "could NOT generate gap textures" << std::endl;
        return ret;
    }
    //delete random filler
    delete[] randomData;
    return 0;
}

int reset_GapTexture() {
    //remove it
    glDeleteTextures(1, &gapTexture);
    gapTexture = 0;
    return init_GapTexture();
}

int init_StripFBO() {
    int ret;
    GLuint lastFBO;

    if (0 != (ret = create_render_buffer(&stripRBO, GL_DEPTH_COMPONENT, stripWidth, stripHeight))) {
        log_error << "could NOT generate RBO" << std::endl;
        return ret;
    }
    if (0 != (ret = create_framebuffer(&stripFBO))) {
        log_error << "could NOT generate FBO" << std::endl;
        glDeleteRenderbuffersEXT(1, &stripRBO);
        stripRBO = 0;
        return ret;
    }

    glGetIntegerv(GL_FRAMEBUFFER_BINDING_EXT, (GLint *) &lastFBO);

    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, stripFBO);
    glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_RENDERBUFFER_EXT, stripRBO);

    //no texture has been attached - this happens later
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, lastFBO);
    return 0;
}

int reset_StripFBO() {
    glDeleteRenderbuffersEXT(1, &stripRBO);
    glDeleteFramebuffersEXT(1, &stripFBO);
    stripRBO = stripFBO = 0;
    return init_StripFBO();
}

int init_Skews() {
    int ret;

    // Right skew
    if (0 != (ret = create_textured_framebuffer(&rightSkewFBO,
                                                &rightSkewDepth, &rightSkewTexture,
                                                stripWidth * (startingStrips + 6), stripHeight))) {
        log_error << "could NOT generate right skew for FBO" << std::endl;
        return ret;
    }
    // Left skew
    if (0 != (ret = create_textured_framebuffer(&leftSkewFBO,
                                                &leftSkewDepth, &leftSkewTexture,
                                                stripWidth * (startingStrips + 6), stripHeight))) {
        log_error << "could NOT generate left skew for FBO" << std::endl;
        glDeleteFramebuffersEXT(1, &rightSkewFBO);
        glDeleteTextures(1, &rightSkewTexture);
        glDeleteTextures(1, &rightSkewDepth);
        rightSkewFBO = rightSkewDepth = rightSkewTexture = 0;
        return ret;
    }
    return 0;
}

int reset_skews() {
    // Do the hovering...
    glDeleteFramebuffersEXT(1, &rightSkewFBO);
    glDeleteTextures(1, &rightSkewTexture);
    glDeleteTextures(1, &rightSkewDepth);
    glDeleteFramebuffersEXT(1, &leftSkewFBO);
    glDeleteTextures(1, &leftSkewTexture);
    glDeleteTextures(1, &leftSkewDepth);
    rightSkewFBO = rightSkewDepth = rightSkewTexture = 0;
    leftSkewFBO = leftSkewDepth = leftSkewTexture = 0;

    return init_Skews();
}

int init_OneWay() {
    int ret;

    if (0 != (ret = generate_shaders(&leftOneWayProgram,
                                     &leftOneWayVS, "magli_leftoneway.vs",
                                     &leftOneWayFS, "magli_leftoneway.fs"))) {
        shader_load_error("magli_leftoneway.vs,magli_leftoneway.fs")
        return ret;
    }

    if (0 != (ret = generate_shaders(&rightOneWayProgram,
                                     &rightOneWayVS, "magli_rightoneway.vs",
                                     &rightOneWayFS, "magli_rightoneway.fs"))) {
        shader_load_error("magli_rightoneway.vs,magli_rightoneway.fs")
        return ret;
    }
    return 0;
}

int init_MapTex() {
    int ret;

    if (0 != (ret = generate_shaders(&seedFillerProgram,
                                     &seedFillerVS, "magli_seedfiller.vs",
                                     &seedFillerFS, "magli_seedfiller.fs"))) {
        shader_load_error("magli_seedfiller.vs,magli_seedfiller.fs")
        return ret;
    }
    if (0 != (ret = generate_shaders(&mixerProgram,
                                     &mixerVS, "magli_mixer.vs",
                                     &mixerFS, "magli_mixer.fs"))) {
        shader_load_error("magli_mixer.vs,magli_mixer.fs")
        return ret;
    }
    if (0 != (ret = generate_shaders(&rightMapTexProgram,
                                     &rightMapTexVS, "magli_rightmaptex.vs",
                                     &rightMapTexFS, "magli_rightmaptex.fs"))) {
        shader_load_error("magli_rightmaptex.vs,magli_rightmaptex.fs")
        return ret;
    }
    if (0 != (ret = generate_shaders(&leftMapTexProgram,
                                     &leftMapTexVS, "magli_leftmaptex.vs",
                                     &leftMapTexFS, "magli_leftmaptex.fs"))) {
        shader_load_error("magli_leftmaptex.vs,magli_leftmaptex.fs")
        return ret;
    }
    return 0;
}

bool set_StripHeight(int height) {
    if (height < 5) {
        log_warning << "strip width too small (< 5)" << std::endl;
        return false;
    }
    stripHeight = height;

    // TODO: srand
    if (is_init) {
        // Reset some things...
        if (0 != reset_StripTextures())
            return false;
        if (0 != reset_SeedTextures())
            return false;
        if (0 != reset_GapTexture())
            return false;
        if (0 != reset_StripFBO())
            return false;
        if (0 != reset_skews())
            return false;
    }

    return true;
}

bool set_StartingStrips(int ss) {
    if (ss < 2) {
        log_warning << "starting strips too small (< 2)" << std::endl;
        return false;
    }
    startingStrips = ss;

    if (is_init) {
        // Reset some things...
        if (0 != reset_StripTextures())
            return false;
        if (0 != reset_GapTexture())
            return false;
        if (0 != reset_skews())
            return false;
    }

    return true;
}

int init_shaders() {
    if (is_init) { // shaders already initialised; ignore.
        log_warning << "shaders are already init!" << std::endl;
        return 0;
    }

    if (0 != init_StripTextures())
        return 1;
    if (0 != init_SeedTextures())
        return 2;
    if (0 != init_GapTexture())
        return 3;
    if (0 != init_StripFBO())
        return 4;
    if (0 != init_Skews())
        return 5;
    if (0 != init_OneWay())
        return 6;
    if (0 != init_MapTex())
        return 7;

    is_init = true; // raise the flag

    return 0;
}

int getStripWidth() {
    return stripWidth;
}

int getStripHeight() {
    return stripHeight;
}

int getStartingStrips() {
    return startingStrips;
}

bool toggleRenderRightOneWay() {
    return (renderRightOneWay = !renderRightOneWay);
}

void sketchRawStrips(bool right) {
    //sketch all the meant-to-be-visible strips as-are
    GLuint *stripTextures = (right ? rightStripTextures : leftStripTextures);

    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    glOrtho(0.0, 1.0, 0.0, 1.0, 0.5, 1.5);

    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();

    float xstart = 0.0f;
    float xend;

    glColor3f(1.0f, 1.0f, 1.0f);
    for (int i = 0; i < lenStripTextures; i++) {
        //glColor3f(1.0f, 1.0f, i / float(lenStripTextures));
        glBindTexture(GL_TEXTURE_2D, stripTextures[i]);
        xend = (i + 1) / float(lenStripTextures);
        glBegin(GL_QUADS);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(xstart, 0.0f, -1.0f);
        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(xend, 0.0f, -1.0f);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(xend, 1.0f, -1.0f);
        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(xstart, 1.0f, -1.0f);
        glEnd();
        xstart = xend;
    }
    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
    glPopAttrib();
}

void sketchSkew() {
    //Sketch the relevant skew
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    glOrtho(0.0, 1.0, 0.0, 1.0, 0.5, 1.5);

    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();

    float xstart;
    float xend;

    glColor3f(1.0f, 1.0f, 1.0f);
    xstart = static_cast<float>(2.0 / (startingStrips + 6));
    xend = static_cast<float>((startingStrips + 4.0) / (startingStrips + 6));
    GLuint desiredSkew = (renderRightOneWay ?
                          rightSkewTexture : leftSkewTexture);
    glBindTexture(GL_TEXTURE_2D, desiredSkew);
    glBegin(GL_QUADS);
    glTexCoord2f(xstart, 0.0f);
    glVertex3f(0.0f, 0.0f, -1.0f);
    glTexCoord2f(xend, 0.0f);
    glVertex3f(1.0f, 0.0f, -1.0f);
    glTexCoord2f(xend, 1.0f);
    glVertex3f(1.0f, 1.0f, -1.0f);
    glTexCoord2f(xstart, 1.0f);
    glVertex3f(0.0f, 1.0f, -1.0f);
    glEnd();
    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
    glPopAttrib();
}

void sketchSkewDepth() {
    //Sketch the relevant skew depth
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    glOrtho(0.0, 1.0, 0.0, 1.0, 0.5, 1.5);

    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();

    float xstart;
    float xend;

    glColor3f(1.0f, 1.0f, 1.0f);
    xstart = static_cast<float>(2.0 / (startingStrips + 6));
    xend = static_cast<float>((startingStrips + 4.0) / (startingStrips + 6));
    GLuint desiredSkew = (renderRightOneWay ?
                          rightSkewDepth : leftSkewDepth);
    glBindTexture(GL_TEXTURE_2D, desiredSkew);
    glBegin(GL_QUADS);
    glTexCoord2f(xstart, 0.0f);
    glVertex3f(0.0f, 0.0f, -1.0f);
    glTexCoord2f(xend, 0.0f);
    glVertex3f(1.0f, 0.0f, -1.0f);
    glTexCoord2f(xend, 1.0f);
    glVertex3f(1.0f, 1.0f, -1.0f);
    glTexCoord2f(xstart, 1.0f);
    glVertex3f(0.0f, 1.0f, -1.0f);
    glEnd();
    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
    glPopAttrib();
}

GLUT_displayFunc stereogramDisplay(GLUT_displayFunc fun) {
    stereogramDisplayFunc = fun;
    return []() -> void {
        // Compute output
        if (!isReady()) {
            log_fatal << "stereogram rendering was not initialized. Please ensure stereogramInit() ran successfully!"
                      << std::endl;
            return;
        }

        if (!stereogramDisplayFunc) {
            renderWarning();
            return;
        }

        //this is where the meaty algorithmic bits will go / be called from
        computeSkews(stereogramDisplayFunc);
        computeOneWay(renderRightOneWay);

        // Sketch output
        //Draws the output of the program. It does not do the final
        //output to screen, so that the output may actually be to
        //a texture or something else.

        //IMPORTANT: this DOES NOT calculate the output. It only
        //displays the last calculated output.

        //There will be issues of whether the output resolution matches
        //up precisely with the scale and number of texture strips.
        //That is all pretty secondary and can be handled later.
        glEnable(GL_TEXTURE_2D);
        glColor3f(1.0f, 1.0f, 1.0f);
        glViewport(0, 0, stripWidth * (startingStrips + 2), stripHeight);

        if (getDisplayMode() == 0) {
            sketchRawStrips(renderRightOneWay);
        } else if (getDisplayMode() == 1)
            sketchSkew();
        else if (getDisplayMode() == 2)
            sketchSkewDepth();
        else {
            glDisable(GL_TEXTURE_2D);
            renderWarning();
            glEnable(GL_TEXTURE_2D);
        }
        glDisable(GL_TEXTURE_2D);

        glutSwapBuffers();
    };
}

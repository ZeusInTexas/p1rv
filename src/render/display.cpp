/*
 * display.cpp: OpenGL display functions for OpenStereo
 *
 * Copyright (c) 2018 Matis Granger, Gustave Bainier
 * Copyright (c) 2011 Daniel Schroeder
 *
 * This file is adapted from MaGLi.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * File created on 16/11/18.
 *
*/

#if _WIN32

#include <Windows.h>

#endif

#include <GL/glew.h>
#include <GL/glut.h>

#include <iostream>

#include "render/renderer.h"
#include <signal.h>
#include "utils/log.h"

using namespace std;

int desiredWidth = 1000;
int desiredHeight = 600;
const int MAX_RESIZE_COUNTER = 15;

bool debugEnable = false;
bool windowResized = false;
unsigned int resizeCounter = 0;

int window = 0;
int wxr;
int wyr;

bool hasCalledInit = false;

//Toggling what is displayed
int displayMode = 0;

int init_shaders();

bool isReady() {
    return hasCalledInit;
}

int getDisplayMode() {
    return displayMode;
}

// Functions static storage
GLUT_reshapeFunc stereogramReshapeFunc;
#if OPENSTEREO_DEV_ENABLE
GLUT_keyboardFunc stereogramKeyboardFunc;
#endif

GLUT_reshapeFunc stereogramReshape(GLUT_reshapeFunc fun) {
    stereogramReshapeFunc = fun;
    return [](int width, int height) -> void {
        if ((width != desiredWidth) || (height != desiredHeight)) {
            windowResized = true;
            resizeCounter = 0;
            desiredWidth = width;
            desiredHeight = height;
        }
        if (stereogramReshapeFunc)
            stereogramReshapeFunc(width, height);
    };
}

GLUT_keyboardFunc stereogramKeyboard(GLUT_keyboardFunc fun) {
#if OPENSTEREO_DEV_ENABLE
    stereogramKeyboardFunc = fun;
    return [](unsigned char key, int x, int y) -> void {
        if (!debugEnable)
            switch (key) {
                case '<':
                    debugEnable = true;
                    log_debug << "enabled dev mode." << std::endl;
                    break;
            }
        else {
            if ((key >= '0') && (key <= '9')) {
                displayMode = (int) (key - '0');
                return;
            }

            switch (key) {
                case '<':
                    debugEnable = false;
                    log_info << "disabled dev mode." << std::endl;
                    break;
                case 'q':
                    stereogramDestroy();
                    exit(0);
                case 'r':
                    toggleRenderRightOneWay();
                    glutPostRedisplay();
                    break;
            }
        }
        if (stereogramKeyboardFunc)
            stereogramKeyboardFunc(key, x, y);
    };
#else
    return fun;
#endif
}

GLUT_motionFunc stereogramMotion(GLUT_motionFunc fun) {
    return fun;
}

GLUT_mouseFunc stereogramMouse(GLUT_mouseFunc fun) {
    return fun;
}

void resizeToFit(int desiredWidth, int desiredHeight) {
    int desiredStrips = desiredWidth / getStripWidth() + 1;
    set_StripHeight(desiredHeight);
    set_StartingStrips(desiredStrips - 2);
}

void stereogramTimer(int) {
    if (windowResized) {
        resizeCounter++;
        if (resizeCounter >= MAX_RESIZE_COUNTER) {
            resizeToFit(desiredWidth, desiredHeight);
            //resize this window to match
            glutReshapeWindow(desiredWidth, desiredHeight);
            windowResized = false;
            resizeCounter = 0;
        }
    }
    glutPostRedisplay();
    glutTimerFunc(33, stereogramTimer, 1);
}

void stereogramDestroy() {
    glutDestroyWindow(window);
}

void _stereogramDeathHandle(int) {
    stereogramDestroy();
}

bool stereogramInit(int *argc, char **argv) {
    // Register signals so we can handle program exit
    signal(SIGINT, _stereogramDeathHandle);
#ifdef __unix__
    signal(SIGKILL, _stereogramDeathHandle);
#endif
    signal(SIGTERM, _stereogramDeathHandle);

    resizeToFit(desiredWidth, desiredHeight);
    wxr = getStripWidth() * (getStartingStrips() + 2);
    wyr = getStripHeight();

    glutInit(argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
    glutInitWindowSize(wxr, wyr);

    window = glutCreateWindow(argv[0]);

    if (glewInit() != GLEW_OK) {
        log_fatal << "GLEW init failed" << endl;
        return false;
    }

    if (!GLEW_EXT_framebuffer_object || !GLEW_ARB_shading_language_100 || !GLEW_ARB_shader_objects) {
        log_fatal << "system does not support framebuffers and/or shaders" << endl;
        return false;
    }

    if (0 != init_shaders()) // Init shaders
    {
        log_fatal << "unable to init GL shading" << endl;
        return false;
    }

    glutTimerFunc(33, stereogramTimer, 1);

    hasCalledInit = true;
    return true;
}

int getWinXRes() {
    return wxr;
}

int getWinYRes() {
    return wyr;
}
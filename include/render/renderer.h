/*
 * renderer.h: OpenGL autostereogram renderer management definitions
 *
 * Copyright (c) 2018 Matis Granger, Gustave Bainier
 * Copyright (c) 2011 Daniel Schroeder
 *
 * This file is adapted from MaGLi.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * File created on 16/11/18.
 *
*/

#ifndef OPENSTEREO_RENDERER_RENDERER_H
#define OPENSTEREO_RENDERER_RENDERER_H

#include "openstereo.h"

bool set_StripHeight(int height);

bool set_StartingStrips(int height);

// Utilities
int getStripWidth();

int getStripHeight();

int getStartingStrips();

int getDisplayMode();

bool toggleRenderRightOneWay();

bool isReady();

//GL util
void renderWarning();

#endif //OPENSTEREO_RENDERER_RENDERER_H
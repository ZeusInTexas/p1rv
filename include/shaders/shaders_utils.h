/*
 * shaders_util.h: Utilities definitions for OpenGL shaders generation and compilation
 *
 * Copyright (c) 2018 Matis Granger, Gustave Bainier
 * Copyright (c) 2011 Daniel Schroeder
 *
 * This file is adapted from MaGLi.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * File created on 10/10/18.
 *
*/

#ifndef OPENSTEREO_SHADERS_SHADERS_UTILS_H
#define OPENSTEREO_SHADERS_SHADERS_UTILS_H

#if _WIN32

#include <Windows.h>

#endif

#include <GL/glew.h>
#include <GL/glut.h>

/**
	Generates a shader program
	@return non-zero value on error
**/
int generate_shaders(GLuint *programptr, GLuint *vsptr, const char *vsfname, GLuint *fsptr, const char *fsfname);

#endif //OPENSTEREO_SHADERS_SHADERS_UTILS_H